# Movie Characters API
Created by Sebastian Hedlund and Fredrik Jonsson during the "Scania #Reskilling programme 2022".

## About this assignment

This assignment is a PostgreSQL database created by using Hibernate, consisting of movies,
their associated characters and franchises they belong to. The database is constructed in Spring Web,
and then exposed through a RESTful Web API to allow users to manipulate the data.

Certain criteria had to be met concerning the construction of the database, regarding linking different tables to one another.
One movie contains many characters, also a character can play in multiple movies. 
One movie belongs to one franchise, but a franchise can contain many movies.


## Installation

Programs necessary for running and operating the database, and it's corresponding API:

* Program for operating the database server, preferably "pgAdmin".
* "Docker Desktop" for running a docker instance and container.
* An API testing application, preferably "Postman".
* IDE of choice for Java development, preferably IntelliJ.


#### Docker:

To set up a database server, one must first employ a docker instance and container. To do this, "Docker Desktop" must be installed on the machine.
Bellow is a link to a video regarding installing and operating "Docker Desktop":

https://www.youtube.com/watch?v=pTFZFxd4hOI

To instantiate a container and image for the SQL-database, open up a new terminal on your machine and run the following command:


``` bash
docker run --name my-postgress -p 5432:5432 -e POSTGRES_PASSWORD=YourPasswordHere -d postgres
```

#### pgAdmin:

Login to the server running on the instantiated docker container with the credentials entered during the setup phase seen above.
Thereafter, create a database named assignmentMovies.


#### IntelliJ:

Clone the Spring application repository and change the password in the file named application.properties, 
to the password entered during the process of creating the docker container.

#### Postman:

Import the file Postman_collection.json to Postman to get all the testing endpoints of the API.




## How to run and operate the API:

To generate data to the database server, simply run the Spring application. The server will run on port 8080,
something that has to be considered later on when using the testing endpoints of the API. After data has been generated,
go to the URL below with a web browser of choice, and the available testing endpoints of the API will be displayed.

http://localhost:8080/swagger-ui/index.html#/



