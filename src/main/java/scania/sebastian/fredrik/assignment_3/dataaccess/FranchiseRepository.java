package scania.sebastian.fredrik.assignment_3.dataaccess;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import scania.sebastian.fredrik.assignment_3.models.domain.Franchise;

import java.util.List;

public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
    @Query(value = "SELECT id FROM movie WHERE franchise_id = :franchiseId", nativeQuery = true)
    List<String> findMoviesIdByFranchiseId(@Param("franchiseId") long franchiseId);

    @Query(value =
            "SELECT c.id FROM character c " +
                    "INNER JOIN movie_character mc ON mc.character_id = c.id " +
                    "INNER JOIN movie m ON m.id = mc.movie_id " +
                    "WHERE m.franchise_id = :franchiseId " +
                    "GROUP BY c.id;"
            ,nativeQuery = true)
    List<String> findCharactersIdByFranchiseId(@Param("franchiseId") long franchiseId);
}
