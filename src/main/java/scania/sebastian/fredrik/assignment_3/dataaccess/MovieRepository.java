package scania.sebastian.fredrik.assignment_3.dataaccess;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import scania.sebastian.fredrik.assignment_3.models.domain.Movie;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Long> {
    @Query(value =
            "SELECT c.id FROM character c " +
                    "INNER JOIN movie_character mc ON mc.character_id = c.id " +
                    "INNER JOIN movie m ON m.id = mc.movie_id " +
                    "WHERE m.id = :movieId " +
                    "GROUP BY c.id;"
            ,nativeQuery = true)
    List<String> findCharactersIdByMovieId(@Param("movieId") long movieId);
}
