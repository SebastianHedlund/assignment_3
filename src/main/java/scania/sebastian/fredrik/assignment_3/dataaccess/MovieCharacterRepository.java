package scania.sebastian.fredrik.assignment_3.dataaccess;

import org.springframework.data.jpa.repository.JpaRepository;
import scania.sebastian.fredrik.assignment_3.models.domain.MovieCharacter;

public interface MovieCharacterRepository extends JpaRepository<MovieCharacter, Long> {
}
