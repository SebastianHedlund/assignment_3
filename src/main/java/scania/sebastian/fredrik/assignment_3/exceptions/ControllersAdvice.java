package scania.sebastian.fredrik.assignment_3.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.AlreadyExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.DoesNotExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.InvalidObjectException;
import scania.sebastian.fredrik.assignment_3.models.exceptionmessage.ExceptionMessage;
import scania.sebastian.fredrik.assignment_3.models.exceptionmessage.ExceptionMessageSingleton;

@ControllerAdvice
public class ControllersAdvice {

    @ExceptionHandler(InvalidObjectException.class)
    protected ResponseEntity<ExceptionMessage> handleInvalidObject(InvalidObjectException invalidObjectException){
        ExceptionMessageSingleton.INSTANCE.setException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, invalidObjectException.getMessage());
        return new ResponseEntity<>(ExceptionMessageSingleton.INSTANCE.getException(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AlreadyExistException.class)
    protected ResponseEntity<ExceptionMessage> handleAlreadyExists(AlreadyExistException alreadyExistException){
        ExceptionMessageSingleton.INSTANCE.setException(HttpStatus.CONFLICT.value(), HttpStatus.CONFLICT, alreadyExistException.getMessage());
        return new ResponseEntity<>(ExceptionMessageSingleton.INSTANCE.getException(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(DoesNotExistException.class)
    protected ResponseEntity<ExceptionMessage> handleDoesNotExists(DoesNotExistException doesNotExistException){
        ExceptionMessageSingleton.INSTANCE.setException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, doesNotExistException.getMessage());
        return new ResponseEntity<>(ExceptionMessageSingleton.INSTANCE.getException(), HttpStatus.NOT_FOUND);
    }

}
