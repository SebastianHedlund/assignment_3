package scania.sebastian.fredrik.assignment_3.exceptions.customexceptions;

public class AlreadyExistException extends MoviesException{
    public AlreadyExistException(String message){
        super(message);
    }
}
