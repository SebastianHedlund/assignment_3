package scania.sebastian.fredrik.assignment_3.exceptions.customexceptions;

public class MoviesException extends Exception{
    public MoviesException(String message) {
        super(message);
    }
}
