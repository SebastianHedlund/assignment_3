package scania.sebastian.fredrik.assignment_3.exceptions.customexceptions;

public class DoesNotExistException extends MoviesException{
    public DoesNotExistException(String message){
        super(message);
    }
}
