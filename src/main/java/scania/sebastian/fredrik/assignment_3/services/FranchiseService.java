package scania.sebastian.fredrik.assignment_3.services;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scania.sebastian.fredrik.assignment_3.dataaccess.FranchiseRepository;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.AlreadyExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.DoesNotExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.InvalidObjectException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.MoviesException;
import scania.sebastian.fredrik.assignment_3.models.domain.Franchise;
import scania.sebastian.fredrik.assignment_3.models.dto.CharactersListDto;
import scania.sebastian.fredrik.assignment_3.models.dto.MoviesInFranchiseDto;
import scania.sebastian.fredrik.assignment_3.models.mapper.CharactersListDtoMapper;
import scania.sebastian.fredrik.assignment_3.models.mapper.MoviesInFranchiseDtoMapper;

import java.util.List;

@Getter
@Service
public class FranchiseService {
    @Autowired
    FranchiseRepository franchiseRepository;
    @Autowired
    MoviesInFranchiseDtoMapper moviesInFranchiseDtoMapper;
    @Autowired
    CharactersListDtoMapper charactersListDtoMapper;

    public List<Franchise> getAllFranchises(){
        return franchiseRepository.findAll();
    }

    public Franchise getFranchise(Long id) throws DoesNotExistException {
        checkFranchiseExist(id);
        return franchiseRepository.findById(id).get();
    }

    public Franchise addFranchise(Franchise franchise) throws AlreadyExistException {
        //Don't allow overwriting an existing ID
        if (franchise.getId() != null)
            checkFranchiseAlreadyExist(franchise.getId());
        return franchiseRepository.save(franchise);
    }

    public Franchise updateFranchise(Franchise franchise, Long id) throws InvalidObjectException {
        checkMatchingId(franchise.getId(), id);
        return franchiseRepository.save(franchise);
    }

    public void deleteFranchise(Long id) throws DoesNotExistException {
        checkFranchiseExist(id);
        franchiseRepository.deleteById(id);
    }

    public Franchise patchFranchise(Franchise franchise, Long id) throws MoviesException {
        checkFranchiseExist(id);
        checkMatchingId(franchise.getId(), id);
        Franchise currentFranchise = franchiseRepository.findById(franchise.getId()).get();

        //Copy state from current object for all parameters the user doesn't provide.
        if (franchise.getName() == null)
            franchise.setName(currentFranchise.getName());
        if (franchise.getDescription() == null)
            franchise.setDescription(currentFranchise.getDescription());
        if (franchise.getMovies() == null)
            franchise.setMovies(currentFranchise.getMovies());

        return franchiseRepository.save(franchise);
    }

    public MoviesInFranchiseDto getAllMoviesFromFranchise(Long id) throws DoesNotExistException {
        checkFranchiseExist(id);
        List<String> movies = franchiseRepository.findMoviesIdByFranchiseId(id);
        return moviesInFranchiseDtoMapper.mapMoviesInFranchiseDto(movies);
    }

    public CharactersListDto getAllCharactersFromFranchise(Long id) throws DoesNotExistException {
        checkFranchiseExist(id);
        List<String> characters = franchiseRepository.findCharactersIdByFranchiseId(id);
        return charactersListDtoMapper.mapCharactersListDto(characters);
    }

    //Input Validators
    public void checkFranchiseExist(Long id) throws DoesNotExistException {
        if (!franchiseRepository.existsById(id))
            throw new DoesNotExistException("Franchise does not exist");
    }

    public void checkFranchiseAlreadyExist(Long id) throws AlreadyExistException {
        if (franchiseRepository.existsById(id))
            throw new AlreadyExistException("Franchise with provided ID already exist");
    }

    public void checkMatchingId(Long bodyId, Long franchiseId) throws InvalidObjectException {
        if (!franchiseId.equals(bodyId))
            throw new InvalidObjectException("Franchise ID does not match in URI and Body");
    }

}
