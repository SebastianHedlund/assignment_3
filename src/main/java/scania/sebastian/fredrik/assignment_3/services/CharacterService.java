package scania.sebastian.fredrik.assignment_3.services;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scania.sebastian.fredrik.assignment_3.dataaccess.MovieCharacterRepository;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.AlreadyExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.DoesNotExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.InvalidObjectException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.MoviesException;
import scania.sebastian.fredrik.assignment_3.models.domain.MovieCharacter;

import java.util.List;

@Getter
@Service
public class CharacterService {
    @Autowired
    MovieCharacterRepository movieCharacterRepository;

    public List<MovieCharacter> getAllCharacters(){
        return movieCharacterRepository.findAll();
    }

    public MovieCharacter getCharacter(Long id) throws DoesNotExistException {
        checkCharacterExist(id);
        return movieCharacterRepository.findById(id).get();
    }

    public MovieCharacter addCharacter(MovieCharacter movieCharacter) throws AlreadyExistException {
        //Don't allow overwriting an existing ID
        if (movieCharacter.getId() != null)
            checkCharacterAlreadyExist(movieCharacter.getId());
        return movieCharacterRepository.save(movieCharacter);
    }

    public MovieCharacter updateCharacter(MovieCharacter movieCharacter, Long id) throws InvalidObjectException {
        checkMatchingId(movieCharacter.getId(), id);
        return movieCharacterRepository.save(movieCharacter);
    }

    public void deleteCharacter(Long id) throws DoesNotExistException {
        checkCharacterExist(id);
        movieCharacterRepository.deleteById(id);
    }

    public MovieCharacter patchCharacter(MovieCharacter movieCharacter, Long id) throws MoviesException {
        checkCharacterExist(id);
        checkMatchingId(movieCharacter.getId(), id);
        MovieCharacter currentCharacter = movieCharacterRepository.findById(movieCharacter.getId()).get();

        //Copy state from current object for all parameters the user doesn't provide.
        if (movieCharacter.getAlias() == null)
            movieCharacter.setAlias(currentCharacter.getAlias());
        if (movieCharacter.getFullName() == null)
            movieCharacter.setFullName(currentCharacter.getFullName());
        if (movieCharacter.getMovies() == null)
            movieCharacter.setMovies(currentCharacter.getMovies());
        if (movieCharacter.getPicture() == null)
            movieCharacter.setPicture(currentCharacter.getPicture());
        if (movieCharacter.getGender() == null)
            movieCharacter.setGender(currentCharacter.getGender());

        return movieCharacterRepository.save(movieCharacter);
    }

    //Input Validators
    public void checkCharacterExist(Long id) throws DoesNotExistException {
        if (!movieCharacterRepository.existsById(id))
            throw new DoesNotExistException("Character does not exist");
    }

    public void checkCharacterAlreadyExist(Long id) throws AlreadyExistException {
        if (movieCharacterRepository.existsById(id))
            throw new AlreadyExistException("Character with provided ID already exist");
    }

    public void checkMatchingId(Long bodyId, Long characterId) throws InvalidObjectException {
        if (!characterId.equals(bodyId))
            throw new InvalidObjectException("Provided ID does not match in URI and Body");
    }

}
