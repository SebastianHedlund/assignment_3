package scania.sebastian.fredrik.assignment_3.services;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scania.sebastian.fredrik.assignment_3.dataaccess.MovieRepository;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.AlreadyExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.DoesNotExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.InvalidObjectException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.MoviesException;
import scania.sebastian.fredrik.assignment_3.models.domain.Movie;
import scania.sebastian.fredrik.assignment_3.models.dto.CharactersListDto;
import scania.sebastian.fredrik.assignment_3.models.mapper.CharactersListDtoMapper;

import java.util.List;

@Getter
@Service
public class MovieService {
    @Autowired
    MovieRepository movieRepository;
    @Autowired
    CharactersListDtoMapper charactersListDtoMapper;

    public List<Movie> getAllMovies(){
        return movieRepository.findAll();
    }

    public Movie getMovie(Long id) throws DoesNotExistException {
        checkMovieExist(id);
        return movieRepository.findById(id).get();
    }

    public Movie addMovie(Movie movie) throws AlreadyExistException {
        //Don't allow overwriting an existing ID
        if (movie.getId() != null)
            checkMovieAlreadyExist(movie.getId());
        return movieRepository.save(movie);
    }

    public Movie updateMovie(Movie movie, Long id) throws InvalidObjectException {
        checkMatchingId(movie.getId(), id);
        return movieRepository.save(movie);
    }

    public void deleteMovie(Long id) throws DoesNotExistException {
        checkMovieExist(id);
        movieRepository.deleteById(id);
    }

    public Movie patchMovie(Movie movie, long id) throws MoviesException {
        checkMovieExist(id);
        checkMatchingId(movie.getId(), id);
        Movie currentMovie = movieRepository.findById(movie.getId()).get();

        //Copy state from current object for all parameters the user doesn't provide.
        if(movie.getMovieTitle()== null)
            movie.setMovieTitle(currentMovie.getMovieTitle());
        if(movie.getReleaseYear()== 0)
            movie.setReleaseYear(currentMovie.getReleaseYear());
        if(movie.getDirector()== null)
            movie.setDirector(currentMovie.getDirector());
        if(movie.getPicture()== null)
            movie.setPicture(currentMovie.getPicture());
        if(movie.getTrailer()== null)
            movie.setTrailer(currentMovie.getTrailer());
        if(movie.getGenre()== null)
            movie.setGenre(currentMovie.getGenre());
        if(movie.getFranchise()== null)
            movie.setFranchise(currentMovie.getFranchise());
        if(movie.getCharacters()== null)
            movie.setCharacters(currentMovie.getCharacters());

        return movieRepository.save(movie);
    }

    public CharactersListDto getAllCharactersFromMovie(Long id) throws DoesNotExistException {
        checkMovieExist(id);
        List<String> characters = movieRepository.findCharactersIdByMovieId(id);
        return charactersListDtoMapper.mapCharactersListDto(characters);
    }

    //Input Validators
    public void checkMovieExist(Long id) throws DoesNotExistException {
        if (!movieRepository.existsById(id))
            throw new DoesNotExistException("Movie does not exist");
    }

    public void checkMovieAlreadyExist(Long id) throws AlreadyExistException {
        if (movieRepository.existsById(id))
            throw new AlreadyExistException("Movie with provided ID already exist");
    }

    public void checkMatchingId(Long bodyId, Long movieId) throws InvalidObjectException {
        if (!movieId.equals(bodyId))
            throw new InvalidObjectException("Movie ID does not match in URI and Body");
    }

}
