package scania.sebastian.fredrik.assignment_3.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.AlreadyExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.DoesNotExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.InvalidObjectException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.MoviesException;
import scania.sebastian.fredrik.assignment_3.models.domain.Movie;
import scania.sebastian.fredrik.assignment_3.models.dto.CharactersListDto;
import scania.sebastian.fredrik.assignment_3.services.MovieService;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movies")
@Tag(name= "Movies", description = "This controls operations for movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @Operation(summary = "This gets all movies")
    @GetMapping()
    public ResponseEntity<List<Movie>> getAllMovies(){
        return new ResponseEntity<>(movieService.getAllMovies(), HttpStatus.OK);
    }

    @Operation(summary = "This gets a movie with it's corresponding ID")
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie(@PathVariable Long id) throws DoesNotExistException {
        return new ResponseEntity<>(movieService.getMovie(id), HttpStatus.OK);
    }

    @Operation(summary = "This adds a new movie to the API")
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) throws AlreadyExistException {
        return new ResponseEntity<>(movieService.addMovie(movie), HttpStatus.CREATED);
    }

    @Operation(summary = "This updates info of an existing movie in the API")
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie) throws InvalidObjectException {
        return new ResponseEntity<>(movieService.updateMovie(movie, id), HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "This partially updates info of an existing movie in the API")
    @PatchMapping("/{id}")
    public ResponseEntity<Movie> updatePartiallyMovie(@PathVariable Long id, @RequestBody Movie movie) throws MoviesException {
        return new ResponseEntity<>(movieService.patchMovie(movie, id), HttpStatus.OK);
    }

    @Operation(summary = "This deletes an existing movie in the API")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMovie(@PathVariable Long id) throws DoesNotExistException {
        movieService.deleteMovie(id);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "This return all characters from a specific movie")
    @GetMapping("/{id}/characters")
    public ResponseEntity<CharactersListDto> getAllCharactersFromMovie(@PathVariable Long id) throws DoesNotExistException {
        return new ResponseEntity<>(movieService.getAllCharactersFromMovie(id), HttpStatus.OK);
    }

}
