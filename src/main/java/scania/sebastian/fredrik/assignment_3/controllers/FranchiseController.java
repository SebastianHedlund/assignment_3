package scania.sebastian.fredrik.assignment_3.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.AlreadyExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.DoesNotExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.InvalidObjectException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.MoviesException;
import scania.sebastian.fredrik.assignment_3.models.domain.Franchise;
import scania.sebastian.fredrik.assignment_3.models.dto.CharactersListDto;
import scania.sebastian.fredrik.assignment_3.models.dto.MoviesInFranchiseDto;
import scania.sebastian.fredrik.assignment_3.services.FranchiseService;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/franchises")
@Tag(name= "Franchises", description = "This controls operations for franchise")
public class FranchiseController {

    @Autowired
    private FranchiseService franchiseService;

    @Operation(summary = "This gets all franchises")
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        return new ResponseEntity<>(franchiseService.getAllFranchises(), HttpStatus.OK);
    }

    @Operation(summary = "This gets the franchise with it's corresponding ID")
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise (@PathVariable Long id) throws DoesNotExistException {
        return new ResponseEntity<>(franchiseService.getFranchise(id), HttpStatus.OK);
    }

    @Operation(summary = "This adds a new franchise to the API")
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) throws AlreadyExistException {
        return new ResponseEntity<>(franchiseService.addFranchise(franchise), HttpStatus.CREATED);
    }

    @Operation(summary = "This updates info of an existing franchise in the API")
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise) throws InvalidObjectException {
        return new ResponseEntity<>(franchiseService.updateFranchise(franchise, id), HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "This partially updates info of an existing franchise in the API")
    @PatchMapping("/{id}")
    public ResponseEntity<Franchise> updatePartiallyFranchise(@PathVariable Long id, @RequestBody Franchise franchise) throws MoviesException {
        return new ResponseEntity<>(franchiseService.patchFranchise(franchise, id), HttpStatus.OK);
    }

    @Operation(summary = "This deletes an existing franchise in the API")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFranchise(@PathVariable Long id) throws DoesNotExistException {
        franchiseService.deleteFranchise(id);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "This return all movies from a specific franchise")
    @GetMapping("/{id}/movies")
    public ResponseEntity<MoviesInFranchiseDto> getAllMoviesFromFranchise(@PathVariable Long id) throws DoesNotExistException {
        return new ResponseEntity<>(franchiseService.getAllMoviesFromFranchise(id), HttpStatus.OK);
    }

    @Operation(summary = "This return all characters from a specific franchise")
    @GetMapping("/{id}/characters")
    public ResponseEntity<CharactersListDto> getAllCharactersFromFranchise(@PathVariable Long id) throws DoesNotExistException {
        return new ResponseEntity<>(franchiseService.getAllCharactersFromFranchise(id), HttpStatus.OK);
    }

}

