package scania.sebastian.fredrik.assignment_3.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.AlreadyExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.DoesNotExistException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.InvalidObjectException;
import scania.sebastian.fredrik.assignment_3.exceptions.customexceptions.MoviesException;
import scania.sebastian.fredrik.assignment_3.models.domain.MovieCharacter;
import scania.sebastian.fredrik.assignment_3.services.CharacterService;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters")
@Tag(name= "Characters", description = "This controls operations for characters")
public class CharacterController {

    @Autowired
    private CharacterService characterService;

    @Operation(summary = "This gets all movie characters")
    @GetMapping()
    public ResponseEntity<List<MovieCharacter>> getAllCharacters() {
        return new ResponseEntity<>(characterService.getAllCharacters(), HttpStatus.OK);
    }

    @Operation(summary = "This gets a movie character with his/hers corresponding ID")
    @GetMapping("/{id}")
    public ResponseEntity<MovieCharacter> getCharacter(@PathVariable Long id) throws DoesNotExistException {
        return new ResponseEntity<>(characterService.getCharacter(id), HttpStatus.OK);
    }

    @Operation(summary = "This adds a new movie character to the API")
    @PostMapping
    public ResponseEntity<MovieCharacter> addCharacter(@RequestBody MovieCharacter movieCharacter) throws AlreadyExistException {
        return new ResponseEntity<>(characterService.addCharacter(movieCharacter), HttpStatus.CREATED);
    }

    @Operation(summary = "This updates an existing movie character in the API")
    @PutMapping("/{id}")
    public ResponseEntity<MovieCharacter> updateCharacter(@PathVariable Long id, @RequestBody MovieCharacter movieCharacter) throws InvalidObjectException {
        return new ResponseEntity<>(characterService.updateCharacter(movieCharacter, id), HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "This updates any state of a movie character.")
    @PatchMapping("/{id}")
    public ResponseEntity<MovieCharacter> patchCharacter(@PathVariable Long id, @RequestBody MovieCharacter movieCharacter) throws MoviesException {
        return new ResponseEntity<>(characterService.patchCharacter(movieCharacter, id), HttpStatus.OK);
    }

    @Operation(summary = "This deletes an existing movie character in the API")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCharacter(@PathVariable Long id) throws DoesNotExistException {
        characterService.deleteCharacter(id);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

}