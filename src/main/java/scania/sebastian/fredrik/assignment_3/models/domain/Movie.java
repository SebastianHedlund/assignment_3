package scania.sebastian.fredrik.assignment_3.models.domain;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 100, nullable = false)
    private String movieTitle;
    @Column()
    private int releaseYear;
    @Column(length = 50)
    private String director;
    private String picture;
    private String trailer;
    @Column(length = 50)
    private String genre;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    public Franchise franchise;
    @JsonGetter("franchise")
    public String franchise(){
        if (franchise != null)
            return "/api/v1/franchises/" + franchise.getId();
        return null;
    }

    @ManyToMany
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )

    public List<MovieCharacter> characters;

    @JsonGetter("characters")
    public List<String> characters(){
        if (characters != null){
            return characters.stream().map(character -> {
                return "/api/v1/characters/" + character.getId();
            }).collect(Collectors.toList());
        }
        return null;
    }

}
