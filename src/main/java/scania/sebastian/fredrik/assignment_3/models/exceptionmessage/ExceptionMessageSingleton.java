package scania.sebastian.fredrik.assignment_3.models.exceptionmessage;

import org.springframework.http.HttpStatus;
import java.time.LocalDateTime;

public enum ExceptionMessageSingleton {
    INSTANCE;

    private ExceptionMessage exceptionMessage = new ExceptionMessage();

    public void setException(int status, HttpStatus error, String message){
        exceptionMessage.setTimeStamp(LocalDateTime.now());
        exceptionMessage.setError(error);
        exceptionMessage.setStatus(status);
        exceptionMessage.setMessage(message);
    }

    public ExceptionMessage getException(){
        return INSTANCE.exceptionMessage;
    }
}
