package scania.sebastian.fredrik.assignment_3.models.mapper;

import org.springframework.stereotype.Component;
import scania.sebastian.fredrik.assignment_3.models.dto.CharactersListDto;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CharactersListDtoMapper {

    //Take in a list of character IDs and add URI when creating CharactersList
    public CharactersListDto mapCharactersListDto(List<String> ids){
        return new CharactersListDto(ids
                .stream()
                .map(x-> x = "/api/v1/characters/" + x)
                .collect(Collectors.toList()));
    }
}
