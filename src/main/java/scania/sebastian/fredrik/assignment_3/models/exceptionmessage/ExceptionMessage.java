package scania.sebastian.fredrik.assignment_3.models.exceptionmessage;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@Setter
public class ExceptionMessage {
    private LocalDateTime timeStamp;
    private int status;
    private HttpStatus error;
    private String message;
}
