package scania.sebastian.fredrik.assignment_3.models.mapper;

import org.springframework.stereotype.Component;
import scania.sebastian.fredrik.assignment_3.models.dto.MoviesInFranchiseDto;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MoviesInFranchiseDtoMapper {

    //Take in a list of movie IDs and add URI when creating MoviesInFranchiseDto
    public MoviesInFranchiseDto mapMoviesInFranchiseDto(List<String> movieId){
        return new MoviesInFranchiseDto(movieId
                .stream()
                .map(x-> x = "/api/v1/movies/" + x)
                .collect(Collectors.toList()));
    }
}
