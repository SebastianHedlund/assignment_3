INSERT INTO character (full_name, alias, gender, picture) VALUES ('Ralph', 'Wreck-It Raplh', 'Male', 'https://i.imgur.com/YYOC2lY.jpeg');
INSERT INTO character (full_name, alias, gender, picture) VALUES ('Vanellope von Schweetz', 'Vanellope', 'Female', 'https://i.pinimg.com/736x/9c/eb/cc/9cebcc7ebe99a875c5ecd5a4563bad7a.jpg');
INSERT INTO character (full_name, alias, gender, picture) VALUES ('Emmet Brickowski', 'Emmet', 'Male', 'https://i.imgur.com/jRnxud5.jpeg');
INSERT INTO character (full_name, alias, gender, picture) VALUES ('Bruce Wayne', 'Lego Batman', 'Male', 'https://i.imgur.com/ak9Rq0D.jpeg');

INSERT INTO franchise (name, description) VALUES ('Wreck-It-Ralph', 'A world where video game characters are self-aware');
INSERT INTO franchise (name, description) VALUES ('The Lego Movie', 'A world where legos are self-aware');

INSERT INTO movie (movie_title, release_year, director, picture, trailer, franchise_id, genre) VALUES ('Wreck-It Ralph', 2012, 'Rich Moore', 'https://i.imgur.com/8K70er5.jpeg',  'https://youtu.be/-4TcToU9L3E', 1, 'Comedy');
INSERT INTO movie (movie_title, release_year, director, picture, trailer, franchise_id, genre) VALUES ('Ralph Breaks the Internet', 2018, 'Rich Moore', 'https://i.imgur.com/O5Zr3rP.jpeg',  'https://youtu.be/_BcYBFC6zfY', 1, 'Comedy');
INSERT INTO movie (movie_title, release_year, director, picture, trailer, franchise_id, genre) VALUES ('The Lego Movie', 2014, 'Phil Lord', 'https://m.media-amazon.com/images/M/MV5BMTg4MDk1ODExN15BMl5BanBnXkFtZTgwNzIyNjg3MDE@._V1_.jpg',  'https://youtu.be/fZ_JOBCLF-I', 2, 'Fantasy');
INSERT INTO movie (movie_title, release_year, director, picture, trailer, franchise_id, genre) VALUES ('The Lego Movie 2', 2019, 'Mike Mitchell', 'https://m.media-amazon.com/images/M/MV5BMTkyOTkwNDc1N15BMl5BanBnXkFtZTgwNzkyMzk3NjM@._V1_.jpg',  'https://youtu.be/GZOtIvpR5Lw', 2, 'Fantasy');

INSERT INTO movie_character (movie_id, character_id) VALUES (1, 1);
INSERT INTO movie_character (movie_id, character_id) VALUES (1, 2);
INSERT INTO movie_character (movie_id, character_id) VALUES (2, 1);
INSERT INTO movie_character (movie_id, character_id) VALUES (2, 2);
INSERT INTO movie_character (movie_id, character_id) VALUES (3, 3);
INSERT INTO movie_character (movie_id, character_id) VALUES (3, 4);
INSERT INTO movie_character (movie_id, character_id) VALUES (4, 3);
INSERT INTO movie_character (movie_id, character_id) VALUES (4, 4);
